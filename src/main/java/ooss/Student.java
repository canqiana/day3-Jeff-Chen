package ooss;

import java.util.Objects;

public class Student extends Person {

    private Klass klass;

    public Student(int id, String name, int age) {
        super(id, name, age);
    }

    public Klass getKlass() {
        return klass;
    }

    @Override
    public String introduce() {
        String introduction = super.introduce() + " I am a student.";
        if (this.klass != null) {
            if (this.klass.isLeader(this)) {
                introduction += "I am the leader of class " + klass.getNumber() + ".";
            } else if (!this.klass.isLeader(this) ) {
                introduction += " I am in class " + this.klass.getNumber() + ".";
            }
        }
        return introduction;

    }

    public void join(Klass klass){
        this.klass = klass;
    }

    public boolean isIn(Klass klass){
        return Objects.equals(this.klass, klass);
    }

    public void observe(Person person){
        System.out.println("I am " + this.name + ", student of Class " + ((Student) person).getKlass().getNumber() +
                ". I know " + person.getName() + " become Leader.");
    }
}
