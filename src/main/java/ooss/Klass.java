package ooss;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Klass {
    private int number;
    private Student leader;

    public List<Person> personList = new ArrayList<>();

    public Klass() {
    }

    public Klass(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public void assignLeader(Student student){
        if(student.getKlass() != this){
            System.out.println("It is not one of us.");
        } else {
            this.leader = student;
//            this.personList.add(student);
            publishMessage(student);
        }
    }

    public boolean isLeader(Student student){
        return Objects.equals(this.leader,student);
    }

    public void attach(Person person){
        this.personList.add(person);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Klass klass = (Klass) o;
        return number == klass.number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }
    public void publishMessage(Student student){
        this.personList.forEach(person -> person.observe(student));
    }
}
