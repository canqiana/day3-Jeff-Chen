package ooss;

import java.util.*;
import java.util.stream.Collectors;

public class Teacher extends Person {
    private List<Klass> klasses = new ArrayList<>();
    public Teacher(int id, String name, int age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
        String introduction = super.introduce() + " I am a teacher.";
        if (!this.klasses.isEmpty()) {
            introduction += " I teach Class ";
            for (int i = 0 ; i < klasses.size() ; i++) {
                if (i != klasses.size() - 1) {
                    introduction += klasses.get(i).getNumber() + ", ";
                } else {
                    introduction += klasses.get(i).getNumber() + ".";
                }

            }
        }
        return introduction;

    }

    public void assignTo(Klass klass){
        this.klasses.add(klass);
    }

    public boolean belongsTo(Klass klass){
        return this.klasses.contains(klass);
    }

    public boolean isTeaching(Student student){
        return this.klasses.contains(student.getKlass());
    }

//    public List<Klass> getKlasses() {
//        return klasses;
//    }

    @Override
    public void observe(Person person){
        System.out.println("I am " + this.name + ", teacher of Class " + ((Student) person).getKlass().getNumber()
                + ". I know " + person.getName() + " become Leader.");
    }
}
