Objective: 

1. Code Review : review each other's homework with teammates and write down good points and bad points about it. Finally, choose the best one to showcase.
2. Learn the common stream apis and practice them, such as map, reduce, filter, etc.
3. Learn the idea of Object Oriented programming and its three features: encapsulation, inheritance, and polymorphism. 
4. Practice Object Oriented programming.

Reflective:  Fruitful and Challenging.

Interpretive:  I learn a lot of new knowledge in this day's study，such as stream apis and Object Oriented programming.  I encountered some difficulties in the process of Code Review, I couldn't fully understand my teammates' code in a short time because I wasn't very familiar with java syntax. 

Decisional:  I will practice Java more in the future. I will also actively seek the help of teachers and classmates when I have problem.